﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace WebApplication2
{
    public partial class L3ContentScheduler : System.Web.UI.Page
    {
        //Define connection string
        string connectionString = WebConfigurationManager.ConnectionStrings["FoxPlus"].ConnectionString;
        //string connectionString = "Data Source=OWNER-PC\\SQLEXPRESS;Initial Catalog=FoxPlus;Integrated Security=True;User ID=sa;Password=Vds@dmin1";

        //Function to refresh the L3 data grid
        protected void RefreshL3Grid()
        {
            //Setup conection, query and reader objects
            SqlConnection foxPlusConnection = new SqlConnection(connectionString);
            SqlCommand foxPlusQuery = new SqlCommand();
            SqlDataReader foxPlusReader;

            //Setup query & setup parameters based on UI controls
            foxPlusQuery.Connection = foxPlusConnection;
            string insertSQL;
            insertSQL = "sp_GetL3PlaylistInfoForEdit";

            foxPlusQuery.CommandText = insertSQL;

            //Open connection
            foxPlusConnection.Open();

            //Fire the reader
            foxPlusReader = foxPlusQuery.ExecuteReader();

            //Refresh the grid
            L3Grid.DataSource = foxPlusReader;
            L3Grid.DataBind();

            //Set the grid font sizes
            L3Grid.HeaderStyle.Font.Size = FontUnit.XSmall;

            //Clean up
            foxPlusReader.Close();
            foxPlusConnection.Close();
        }

        //Function for page load
        protected void Page_Load(object sender, EventArgs e)
        {
            //Refresh the data grid
            RefreshL3Grid();

            //Enable/disable controls based on user rights
            if ((User.IsInRole("Administrator")) || (User.IsInRole("L3ContentScheduleEditor")))
            {
                //Clear label & enable controls
                StatusLabel.Text = "";
                StatusLabel.ForeColor = System.Drawing.Color.LimeGreen;

                CntrlPanel.Enabled = true;
                L3Grid.Enabled = true;
            }
            else
            {
                //Force user to login page
                Response.Redirect("~/Account/Login.aspx");
                //Set label & disable controls
                //StatusLabel.ForeColor = System.Drawing.Color.Red;
                //StatusLabel.Text = "You are not logged in!";

                //CntrlPanel.Enabled = false;
                //L3Grid.Enabled = false;
            }

            if (!this.IsPostBack)
            {
                //Setup conection, query and reader objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();
                SqlDataReader foxPlusReader;

                //Setup query & setup parameters based on UI controls
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "SELECT * FROM WeatherGroups";
                foxPlusQuery.CommandText = insertSQL;
                //Open connection
                foxPlusConnection.Open();
                //Fire the reader
                foxPlusReader = foxPlusQuery.ExecuteReader();
                //Refresh the add/insert group drop-down
                GroupDropDownList.DataSource = foxPlusReader;
                GroupDropDownList.DataTextField = "GroupName";
                GroupDropDownList.DataValueField = "GroupName";
                GroupDropDownList.DataBind();
                //Clean up
                foxPlusReader.Close();
                foxPlusConnection.Close();

                //Open connection
                foxPlusConnection.Open();
                //Fire the reader
                foxPlusReader = foxPlusQuery.ExecuteReader();
                //Refresh the edit group drop-down
                GroupDropDownListEdit.DataSource = foxPlusReader;
                GroupDropDownListEdit.DataTextField = "GroupName";
                GroupDropDownListEdit.DataValueField = "GroupName";
                GroupDropDownListEdit.DataBind();
                //Clean up
                foxPlusReader.Close();
                foxPlusConnection.Close();

                //Add the enabled RSS feeds to the data type drop-downs
                insertSQL = "sp_GetRSSFeeds";
                foxPlusQuery.CommandText = insertSQL;
                //Open connection
                foxPlusConnection.Open();
                //Fire the reader
                foxPlusReader = foxPlusQuery.ExecuteReader();
                //Start at type = 6 for individual RSS types
                int counter = 6;
                while (foxPlusReader.Read())
                {
                    //Add the enabled RSS feed types to the type drop-downs
                    TypeDropDownList.Items.Insert(TypeDropDownList.Items.Count, new ListItem(foxPlusReader["DataDescription"].ToString(), foxPlusReader["DataType"].ToString()));
                    TypeDropDownListEdit.Items.Insert(TypeDropDownListEdit.Items.Count, new ListItem(foxPlusReader["DataDescription"].ToString(), foxPlusReader["DataType"].ToString()));
                    counter += 1; 
                }
                //Clean up
                foxPlusReader.Close();
                foxPlusConnection.Close();

                //Init the time drop-downs
                StartTimeHour.SelectedIndex = 0;
                StartTimeMinute.SelectedIndex = 0;

                //Init labels
                EditConfirmLabel.ForeColor = System.Drawing.Color.Red;
                EditConfirmLabel.Text = "No record selected for edit";
            }
        }

        //Handler for grid row data binding - set font to small
        protected void L3Grid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Font.Size = FontUnit.Small;
                e.Row.Cells[0].Width = 60;
                e.Row.Cells[3].Width = 100;
                e.Row.Cells[4].Width = 180;
                e.Row.Cells[5].Width = 80;
                e.Row.Cells[6].Width = 140;
                e.Row.Cells[8].Width = 150;
                //Right-aling Theme start time
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;

                //Set row colors
                Int32 typeVal= Convert.ToInt32(e.Row.Cells[5].Text);
                switch (typeVal)
                {
                    //None
                    case 0:
                        e.Row.BackColor = System.Drawing.Color.RosyBrown;
                        break;
                    //News
                    case 1:
                        e.Row.BackColor = System.Drawing.Color.CornflowerBlue;
                        break;
                    //Sports
                    case 2:
                        e.Row.BackColor = System.Drawing.Color.Silver;
                        break;
                    //Weather
                    case 3:
                        e.Row.BackColor = System.Drawing.Color.Tan;
                        break;
                    //Financial
                    case 4:
                        e.Row.BackColor = System.Drawing.Color.LightGreen;
                        break;
                    //RSS
                    default:
                        e.Row.BackColor = System.Drawing.Color.SlateGray;
                        break;
                }
            }
        }

        //Handler for Add/Insert item control button
        protected void AddItemButton_Click(object sender, EventArgs e)
        {
            //Show panel for add/insert
            this.AddEntryPanel.Visible = true;
            //Make sure edit panel is not visible
            this.EditEntryPanel.Visible = false;
            //Clear status label
            StatusLabel.Text = "";
            ConfirmLabel.Text = "";
            EditConfirmLabel.Text = "";
        }

        //Handler for Edit item control button
        protected void EditItemButton_Click(object sender, EventArgs e)
        {
            //Show panel for edit
            this.EditEntryPanel.Visible = true;
            //Make sure add/insert panel is not visible
            this.AddEntryPanel.Visible = false;
            //Clear status label
            StatusLabel.Text = "";
            ConfirmLabel.Text = "";
            EditConfirmLabel.Text = "";
        }

        //Handler for add entry button
        protected void AddEntryButton_Click(object sender, EventArgs e)
        {
            //Check for valid data entry
            if (PeriodDescription.Text == "")
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must enter a valid time period description!";
                return;
            }

            //Get values needed from stored procedure from grid 
            int dayNumber = DayDropDownList.SelectedIndex + 1;
            string startEnableTime = "1899-12-30 " + StartTimeHour.SelectedItem.Text + ":" + StartTimeMinute.SelectedItem.Text + ":0";
            string timePeriodDescription = PeriodDescription.Text;
            //Need to add 1 to get correct data type
            //int dataType = TypeDropDownList.SelectedIndex+1;
            int dataType = Convert.ToInt32(TypeDropDownList.SelectedItem.Value);
            string dataTypeDescription = TypeDropDownList.SelectedItem.Text;
            int groupID;
            string groupDescription;
            //Check for weather group
            if (Convert.ToInt32(TypeDropDownList.SelectedItem.Value) == 3)
            {
                groupID = GroupDropDownList.SelectedIndex + 1;
                groupDescription = GroupDropDownList.SelectedItem.Text;
            }
            //Check for sports
            else if (Convert.ToInt32(TypeDropDownList.SelectedItem.Value) == 2)
            {
                groupID = 1;
                groupDescription = "";
            }
            else
            {
                groupID = 0;
                groupDescription = "";
            }

            //Call stored procedure to add the record
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_AppendL3ScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += "'" + timePeriodDescription + "', ";
                insertSQL += dataType + ", ";
                insertSQL += "'" + dataTypeDescription + "', ";
                insertSQL += groupID + ", ";
                insertSQL += "'" + groupDescription + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the grid
                RefreshL3Grid();

                //Alert operator
                StatusLabel.Text = "";
                ConfirmLabel.Text = "Entry appended to database";
                ConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                ConfirmLabel.Visible = true;

                //Clear control values
                //PeriodDescription.Text = "";
                //Un-select the row in the grid & clear the text
                L3Grid.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for insert entry button
        protected void InsertEntryButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (L3Grid.SelectedRow == null)
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must select a row in the grid before attempting to insert a new entry!";
                return;
            }

            //Check for valid data entry
            if (PeriodDescription.Text == "")
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must enter a valid time period description!";
                return;
            }

            //Get selected row index for grid
            int rowNum = L3Grid.SelectedRow.RowIndex + 1;

            //Get values needed from stored procedure from grid 
            int entryIndex = Convert.ToInt32(L3Grid.Rows[L3Grid.SelectedIndex].Cells[1].Text);
            int dayNumber = DayDropDownList.SelectedIndex + 1;
            string startEnableTime = "1899-12-30 " + StartTimeHour.SelectedItem.Text + ":" + StartTimeMinute.SelectedItem.Text + ":0";
            string timePeriodDescription = PeriodDescription.Text;
            //Need to add 1 to get correct data type
            //int dataType = TypeDropDownList.SelectedIndex+1;
            int dataType = Convert.ToInt32(TypeDropDownList.SelectedItem.Value);
            string dataTypeDescription = TypeDropDownList.SelectedItem.Text;
            int groupID;
            string groupDescription;
            //Check for weather group
            if (Convert.ToInt32(TypeDropDownList.SelectedItem.Value) == 3)
            {
                groupID = GroupDropDownList.SelectedIndex + 1;
                groupDescription = GroupDropDownList.SelectedItem.Text;
            }
            //Check for sports
            else if (Convert.ToInt32(TypeDropDownList.SelectedItem.Value) == 2)
            {
                groupID = 1;
                groupDescription = "";
            }
            else
            {
                groupID = 0;
                groupDescription = "";
            }

            //Call stored procedure to insert the record
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_InsertL3ScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += entryIndex + ", ";
                insertSQL += "'" + timePeriodDescription + "', ";
                insertSQL += dataType + ", ";
                insertSQL += "'" + dataTypeDescription + "', ";
                insertSQL += groupID + ", ";
                insertSQL += "'" + groupDescription + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the grid
                RefreshL3Grid();

                //Alert operator
                StatusLabel.Text = "";
                ConfirmLabel.Text = "Entry inserted into database";
                ConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                ConfirmLabel.Visible = true;

                //Clear control values
                //PeriodDescription.Text = "";
                //Un-select the row in the grid & clear the text
                L3Grid.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for update entry button
        protected void UpdatEntryButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (L3Grid.SelectedRow == null)
            {
                EditConfirmLabel.ForeColor = System.Drawing.Color.Red;
                EditConfirmLabel.Text = "You must select a row in the grid before attempting to update an entry!";
                return;
            }

            //Check for valid data entry
            if (PeriodDescriptionEdit.Text == "")
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must enter a valid time period description!";
                return;
            }

            //Get selected row index for grid
            int rowNum = L3Grid.SelectedRow.RowIndex + 1;

            //Get values needed for stored procedure from grid 
            int entryIndex = rowNum;           
            
            int dayNumber = 0;
            //int dayNumber = Convert.ToInt32(L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text);
            if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Sun")
            {
                dayNumber = 1;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Mon")
            {
                dayNumber = 2;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Tue")
            {
                dayNumber = 3;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Wed")
            {
                dayNumber = 4;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Thu")
            {
                dayNumber = 5;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Fri")
            {
                dayNumber = 6;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Sat")
            {
                dayNumber = 7;
            }           
            
            string startEnableTime = "1899-12-30 " +
                Convert.ToString(Convert.ToDateTime(L3Grid.Rows[L3Grid.SelectedIndex].Cells[3].Text).Hour) + ":" +
                Convert.ToString(Convert.ToDateTime(L3Grid.Rows[L3Grid.SelectedIndex].Cells[3].Text).Minute) + ":0";
            string timePeriodDescription = PeriodDescriptionEdit.Text;
            //Need to add 1 to get correct data type
            //int dataType = TypeDropDownListEdit.SelectedIndex;
            int dataType = Convert.ToInt32(TypeDropDownListEdit.SelectedItem.Value);
            string dataTypeDescription = TypeDropDownListEdit.SelectedItem.Text;
            int groupID;
            string groupDescription;
            //Check for weather group
            if (Convert.ToInt32(TypeDropDownListEdit.SelectedItem.Value) == 3)
            {
                groupID = GroupDropDownListEdit.SelectedIndex + 1;
                groupDescription = GroupDropDownListEdit.SelectedItem.Text;
            }
            //Check for sports
            else if (Convert.ToInt32(TypeDropDownListEdit.SelectedItem.Value) == 2)
            {
                groupID = 1;
                groupDescription = "";
            }
            else
            {
                groupID = 0;
                groupDescription = "";
            }

            //Call stored procedure to delete the selected row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_UpdateL3ScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += entryIndex + ", ";
                insertSQL += "'" + timePeriodDescription + "', ";
                insertSQL += dataType + ", ";
                insertSQL += "'" + dataTypeDescription + "', ";
                insertSQL += groupID + ", ";
                insertSQL += "'" + groupDescription + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the grid
                RefreshL3Grid();

                //Alert operator
                StatusLabel.Text = "";
                EditConfirmLabel.Text = "Edited entry saved to database. You can select another row to edit.";
                EditConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                EditConfirmLabel.Visible = true;

                //Un-select the row in the grid & clear the text
                L3Grid.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for delete item button
        protected void DeleteItemButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (L3Grid.SelectedRow == null)
            {
                StatusLabel.ForeColor = System.Drawing.Color.Red;
                StatusLabel.Text = "You must select a row in the grid before attempting to delete an entry!";
                return;
            }

            //Get selected row index for grid
            int rowNum = L3Grid.SelectedRow.RowIndex;

            //Get values needed from stored procedure from grid 
            string entryIndex = L3Grid.Rows[rowNum].Cells[1].Text;

            int dayNumber = 0;
            //string dayNumber = L3Grid.Rows[rowNum].Cells[2].Text;
            if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Sun")
            {
                dayNumber = 1;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Mon")
            {
                dayNumber = 2;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Tue")
            {
                dayNumber = 3;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Wed")
            {
                dayNumber = 4;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Thu")
            {
                dayNumber = 5;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Fri")
            {
                dayNumber = 6;
            }
            else if (L3Grid.Rows[L3Grid.SelectedIndex].Cells[2].Text == "Sat")
            {
                dayNumber = 7;
            }           

            string startEnableTime = L3Grid.Rows[rowNum].Cells[3].Text;

            //Call stored procedure to delete the selected row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_DeleteL3ScheduleEntry ";
                insertSQL += dayNumber + ", ";
                //insertSQL += "1" + ", ";
                insertSQL += "'" + "1899-12-30 " + startEnableTime + "', ";
                insertSQL += entryIndex;

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Refresh the grid

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the grid
                RefreshL3Grid();

                //Alert operator & clear other labels
                StatusLabel.Text = "Entry deleted from database";
                StatusLabel.ForeColor = System.Drawing.Color.LimeGreen;
                StatusLabel.Visible = true;
                ConfirmLabel.Text = "";
                EditConfirmLabel.Text = "";

                //Deselect entry in grid
                L3Grid.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for cancel out of Add/Insert record mode
        protected void CancelButton_Click(object sender, EventArgs e)
        {
            AddEntryPanel.Visible = false;
            ConfirmLabel.Text = "";
            //Deselect entry in grid
            L3Grid.SelectedIndex = -1;
            return;
        }

        //Handler for cancel edit button
        protected void CancelEditButton_Click(object sender, EventArgs e)
        {
            EditEntryPanel.Visible = false;
            EditConfirmLabel.Text = "";
            //Deselect entry in grid
            L3Grid.SelectedIndex = -1;
            return;
        }

        //Handler for type select drop-down change
        protected void TypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Check for weather
            if (TypeDropDownListEdit.SelectedIndex == 3)
            {
                GroupLabelEdit.Visible = true;
                GroupDropDownListEdit.Visible = true;
            }
            else
            {
                GroupLabelEdit.Visible = false;
                GroupDropDownListEdit.Visible = false;
            }
        }

        //Handler for type select drop-down change - edit panel
        protected void TypeDropDownListEdit_SelectedIndexChanged1(object sender, EventArgs e)
        {
            if (Convert.ToInt32(TypeDropDownListEdit.SelectedValue) == 3)
            {
                GroupLabelEdit.Visible = true;
                GroupDropDownListEdit.Visible = true;
            }
            else
            {
                GroupLabelEdit.Visible = false;
                GroupDropDownListEdit.Visible = false;
            }
        }

        //Handler for change to selected row in grid - set update edit controls
        protected void L3Grid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set edit panel control values based on currently selected record
            PeriodDescriptionEdit.Text = L3Grid.Rows[L3Grid.SelectedIndex].Cells[4].Text;
            TypeDropDownListEdit.SelectedValue = L3Grid.Rows[L3Grid.SelectedIndex].Cells[5].Text;

            //Check for weather
            if (TypeDropDownListEdit.SelectedIndex == 3)
            {
                GroupDropDownListEdit.Visible = true;
                GroupDropDownListEdit.SelectedItem.Value = L3Grid.Rows[L3Grid.SelectedIndex].Cells[8].Text;
            }
            else
            {
                GroupDropDownListEdit.Visible = false;
            }

            //Set confirm label
            EditConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
            EditConfirmLabel.Text = "Record selected for edit";
        }

        //Handler for change in index to day select drop-down => refreshes grid filtered for selected day
        protected void DataSelectDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            //Setup connection, query and reader objects
            SqlConnection foxPlusConnection = new SqlConnection(connectionString);
            SqlCommand foxPlusQuery = new SqlCommand();
            SqlDataReader foxPlusReader;

            //Setup query & setup parameters based on UI controls
            foxPlusQuery.Connection = foxPlusConnection;
            string insertSQL;
            //Request data for grid - filter for selected day
            insertSQL = "sp_GetL3PlaylistInfoForEdit " + Convert.ToString(DaySelectDropDown.SelectedIndex);

            foxPlusQuery.CommandText = insertSQL;

            //Open connection
            foxPlusConnection.Open();

            //Fire the reader
            foxPlusReader = foxPlusQuery.ExecuteReader();

            //Refresh the grid
            L3Grid.DataSource = foxPlusReader;
            L3Grid.DataBind();

            //Set the grid font sizes
            L3Grid.HeaderStyle.Font.Size = FontUnit.XSmall;

            //Clean up
            foxPlusReader.Close();
            foxPlusConnection.Close();
        }
    }
}