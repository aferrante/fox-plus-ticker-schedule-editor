﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BrandingBugScheduler.aspx.cs" Inherits="WebApplication2.WebForm1" MaintainScrollPositionOnPostback ="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    </asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="MainHeader" runat="server" Text="Branding Bug Scheduler " Font-Bold="True" Font-Size="X-Large"></asp:Label>
    <asp:Panel ID="CntrlPanel" runat="server" BorderStyle="Solid" BorderWidth="2px" Width="625px">
    &nbsp;<asp:Label ID="ControlPanelLabel" runat="server" Text="Control" Font-Bold="True"></asp:Label>
    <br />
    <span style="padding-left:54px">
        <asp:Button ID="AddItemButton" runat="server" Text="Add/Insert Schedule Item" OnClick="AddItemButton_Click" Width="244px" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="EditItemButton" runat="server" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Text="Edit Selected Item" Width="244px" OnClick="EditItemButton_Click" />
    </span>
    <br />
    <span style="padding-left:54px">
        <asp:Button ID="DeleteItemButton" runat="server" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" OnClick="DeleteItemButton_Click" Text="Delete Selected Item" Width="244px" />
        <br />
    </span>
    <br />
    <span style="padding-left:20px">
        <asp:Label ID="StatusLabel" runat="server" Text="" Font-Bold="True" ForeColor="Red"></asp:Label>
    </span>
    </asp:Panel>
    <p>
        Day Codes: 1=Sunday, 2=Monday, 3=Tuesday, 4=Wednesday, 5=Thursday, 6=Friday, 7=Saturday
    </p>
    <p>
        Color Codes: 0=Red, 1=Blue, 3=Gray
    </p>
    <asp:UpdatePanel ID="AppendInsertUpdatePanel" runat="server">
    <ContentTemplate>
    <asp:Panel ID="AddEntryPanel" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="2px" Width="625px">
        &nbsp;<asp:Label ID="HeaderLabel" runat="server" Text="Add/Insert New Schedule Entry " Font-Bold="True"></asp:Label>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="DayNumLabel" runat="server" Text="Day of Week:" Font-Bold="False" Font-Size="Small" Width="140px"></asp:Label>&nbsp;
            <asp:DropDownList ID="DayDropDownList" runat="server" Font-Size="Small" Height="25px" Width="110px">
                <asp:ListItem Value="0">Sunday (1)</asp:ListItem>
                <asp:ListItem Value="1">Monday (2)</asp:ListItem>
                <asp:ListItem Value="2">Tuesday (3)</asp:ListItem>
                <asp:ListItem Value="3">Wednesday (4)</asp:ListItem>
                <asp:ListItem Value="4">Thursday (5)</asp:ListItem>
                <asp:ListItem Value="5">Friday (6)</asp:ListItem>
                <asp:ListItem Value="6">Saturday (7)</asp:ListItem>
            </asp:DropDownList>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="StartTimeLabelHour" runat="server" Text="Start Time (Hour: 0-23):" Font-Bold="False" Font-Size="Small" Width="140px"></asp:Label>&nbsp;
            <asp:DropDownList ID="StartTimeHour" runat="server" Font-Size="Small" Height="25px" Width="110px">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="StartTimeLabelMinute" runat="server" Font-Bold="False" Text="Start Time (Min: 0-59):" Font-Size="Small" Width="135px"></asp:Label>&nbsp;
            <asp:DropDownList ID="StartTimeMinute" runat="server" Font-Size="Small" Height="25px" Width="60px">
            <asp:ListItem>0</asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
            <asp:ListItem>32</asp:ListItem>
            <asp:ListItem>33</asp:ListItem>
            <asp:ListItem>34</asp:ListItem>
            <asp:ListItem>35</asp:ListItem>
            <asp:ListItem>36</asp:ListItem>
            <asp:ListItem>37</asp:ListItem>
            <asp:ListItem>38</asp:ListItem>
            <asp:ListItem>39</asp:ListItem>
            <asp:ListItem>40</asp:ListItem>
            <asp:ListItem>41</asp:ListItem>
            <asp:ListItem>42</asp:ListItem>
            <asp:ListItem>43</asp:ListItem>
            <asp:ListItem>44</asp:ListItem>
            <asp:ListItem>45</asp:ListItem>
            <asp:ListItem>46</asp:ListItem>
            <asp:ListItem>47</asp:ListItem>
            <asp:ListItem>48</asp:ListItem>
            <asp:ListItem>49</asp:ListItem>
            <asp:ListItem>50</asp:ListItem>
            <asp:ListItem>51</asp:ListItem>
            <asp:ListItem>52</asp:ListItem>
            <asp:ListItem>53</asp:ListItem>
            <asp:ListItem>54</asp:ListItem>
            <asp:ListItem>55</asp:ListItem>
            <asp:ListItem>56</asp:ListItem>
            <asp:ListItem>57</asp:ListItem>
            <asp:ListItem>58</asp:ListItem>
            <asp:ListItem>59</asp:ListItem>
        </asp:DropDownList>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="ColorNumLabel" runat="server" Text="Color:" Font-Bold="False" Font-Size="Small"></asp:Label>
            <asp:DropDownList ID="ColorDropDownList" runat="server" Font-Size="Small" Height="25px">
                <asp:ListItem Value="0">Red (0)</asp:ListItem>
                <asp:ListItem Value="1">Blue (1)</asp:ListItem>
                <asp:ListItem Value="2">Gray (2)</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="TypeLabel" runat="server" Text="Type:" Font-Bold="False" Font-Size="Small"></asp:Label>
            <asp:DropDownList ID="TypeDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="TypeDropDownList_SelectedIndexChanged" Font-Size="Small" Height="25px">
                <asp:ListItem Value="0">Simple Text</asp:ListItem>
                <asp:ListItem Value="1">Clock</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="TextLabel" runat="server" Text="Text:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:TextBox ID="TextTextBox" runat="server" BackColor="White" BorderColor="Black" Width="75" Visible="True" Font-Size="Small"></asp:TextBox>
            <asp:Label ID="TimezoneLabel" runat="server" Text="Timezone:" Font-Bold="False" Visible="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:DropDownList ID="TimezoneDropDownList" runat="server" Visible="False" Font-Size="Small" Height="25px">
                <asp:ListItem Value="0">ET</asp:ListItem>
                <asp:ListItem Value="1">CT</asp:ListItem>
                <asp:ListItem Value="2">MT</asp:ListItem>
                <asp:ListItem Value="3">PT</asp:ListItem>
            </asp:DropDownList>
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="EnableLabel" runat="server" Text="Enabled:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:CheckBox ID="EnableCheckBox" runat="server" Checked="True" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Button ID="AddEntryButton" runat="server" Text="Add Entry to Schedule" OnClick="AddEntryButton_Click" BackColor="Silver" Width="200px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="InsertEntryButton" runat="server" Text="Insert Entry into Schedule" OnClick="InsertEntryButton_Click" BackColor="Silver" Width="220px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="CancelButton" runat="server" OnClick="CancelAddInsertButton_Click" Text="Cancel" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Width="80px" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="ConfirmLabel" runat="server" Font-Bold="True" Text="Entry saved to database" Visible="False" ForeColor="#33CC33"></asp:Label>
        </span>
        <br />
    </asp:Panel>

    </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel ID="EditUpdatePanel" runat="server">
    <ContentTemplate>
    <asp:Panel ID="EditEntryPanel" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="2px" Width="625px">
        &nbsp;<asp:Label ID="HeaderLabel0" runat="server" Text="Edit Schedule Entry " Font-Bold="True"></asp:Label>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="ColorNumLabel0" runat="server" Text="Color:" Font-Bold="False" Font-Size="Small"></asp:Label>
            <asp:DropDownList ID="ColorDropDownListEdit" runat="server" Font-Size="Small" Height="25px">
                <asp:ListItem Value="0">Red (0)</asp:ListItem>
                <asp:ListItem Value="1">Blue (1)</asp:ListItem>
                <asp:ListItem Value="2">Gray (2)</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="TypeLabel0" runat="server" Text="Type:" Font-Bold="False" Font-Size="Small"></asp:Label>
            <asp:DropDownList ID="TypeDropDownListEdit" runat="server" AutoPostBack="True" Font-Size="Small" Height="25px" OnSelectedIndexChanged="TypeDropDownListEdit_SelectedIndexChanged">
                <asp:ListItem Value="0">Simple Text</asp:ListItem>
                <asp:ListItem Value="1">Clock</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="TextLabelEdit" runat="server" Text="Text:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:TextBox ID="TextTextBoxEdit" runat="server" BackColor="White" BorderColor="Black" Width="75" Visible="True" Font-Size="Small"></asp:TextBox>
            <asp:Label ID="TimezoneLabelEdit" runat="server" Text="Timezone:" Font-Bold="False" Visible="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:DropDownList ID="TimezoneDropDownListEdit" runat="server" Visible="False" Font-Size="Small" Height="25px">
                <asp:ListItem Value="0">ET</asp:ListItem>
                <asp:ListItem Value="1">CT</asp:ListItem>
                <asp:ListItem Value="2">MT</asp:ListItem>
                <asp:ListItem Value="3">PT</asp:ListItem>
            </asp:DropDownList>
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="EnableLabel0" runat="server" Text="Enabled:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:CheckBox ID="EnableCheckBoxEdit" runat="server" Checked="True" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Button ID="UpdatEntryButton" runat="server" Text="Update Entry" OnClick="UpdatEntryButton_Click" BackColor="Silver" Width="200px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="CancelEditButton" runat="server" OnClick="CancelEditButton_Click" Text="Cancel" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Width="80px" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="EditConfirmLabel" runat="server" Font-Bold="True" Text="Entry saved to database" ForeColor="#33CC33"></asp:Label>
        </span>
        <br />
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>

        <asp:UpdatePanel ID="GridUpdatePanel" runat="server">
            <ContentTemplate>
            <asp:GridView ID="BrandingBugGrid" runat="server" AutoGenerateColumns="False" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" DataKeyNames="DayOfWeek,StartEnableTime,EntryIndex" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" ForeColor="Black" OnSelectedIndexChanged="BrandingBugGrid_SelectedIndexChanged" Caption="Current Fox News Rotator Schedule" CaptionAlign="Left" OnRowDataBound="BrandingBugGrid_RowDataBound" Width="630px">
                <AlternatingRowStyle BorderStyle="Solid" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True">
                    <ControlStyle Font-Size="Small" />
                    <HeaderStyle Font-Size="Smaller" />
                    <ItemStyle Width="90px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="EntryIndex" HeaderText="Index" SortExpression="EntryIndex">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DayOfWeek" HeaderText="Day" SortExpression="DayOfWeek">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="StartEnableTime" DataFormatString="{0: H:mm:ss}" HeaderText="Start Time" SortExpression="StartEnableTime">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ColorValue" HeaderText="Color" SortExpression="ColorValue">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TextLabel" HeaderText="Text" SortExpression="TextLabel">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="ClockEnable" HeaderText="Clock" SortExpression="ClockEnable">
                    <ControlStyle Font-Bold="True" />
                    <HeaderStyle Font-Size="Small" />
                    </asp:CheckBoxField>
                    <asp:BoundField DataField="TimezoneSuffix" HeaderText="Timezone" SortExpression="TimezoneSuffix">
                    <HeaderStyle Font-Size="Small" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="Enabled" HeaderText="Enable" SortExpression="Enabled">
                    <HeaderStyle Font-Size="Small" />
                    </asp:CheckBoxField>
                </Columns>
                <EditRowStyle Font-Size="Small" />
                <FooterStyle BackColor="#CCCCCC" />
                <HeaderStyle BackColor="Black" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="White" HorizontalAlign="Center" />
                <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="White" BorderColor="Black" Font-Bold="False" BorderStyle="Solid" BorderWidth="2px" />
                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="Gray" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#383838" />
            </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
</asp:Content>
