﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication2._Default" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1>
                    <br />
                    FOX Plus Ticker Schedule Editor</h1>
            </hgroup>
            <span style="padding-left:0px">
            <br/>
            <asp:Label ID="StatusLabel" runat="server" Font-Bold="True" Font-Size="Medium"></asp:Label>
            </span>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h3>With this application you can do the following:</h3>
    <ol class="round">
        <li class="one">
            <h5>Modify the Fox News Branding Bug Schedule</h5>
            This is the rotating bug at the left edge of the ticker region.
            <a href="BrandingBugScheduler.aspx">Go to the Branding Bug Schedule Editor…</a>
        </li>
        <li class="two">
            <h5>Modify the Data Bug Schedule</h5>
            This is the data bug that appears at the right-edge of the ticker region.
            <a href="DataBugScheduler.aspx">Go to the Data Bug Schedule Editor…</a>
        </li>
        <li class="three">
            <h5>Modify the Lower-Third Data Schedule</h5>
            This is the main ticker content area.
            <a href="L3ContentScheduler.aspx">Go to the Lower-Third Content Schedule editor…</a>
        </li>
    </ol>
</asp:Content>
