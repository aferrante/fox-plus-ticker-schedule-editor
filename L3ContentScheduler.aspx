﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="L3ContentScheduler.aspx.cs" Inherits="WebApplication2.L3ContentScheduler" MaintainScrollPositionOnPostback ="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label ID="MainHeader" runat="server" Text="Lower-Third Content Scheduler " Font-Bold="True" Font-Size="X-Large"></asp:Label>
    <asp:Panel ID="CntrlPanel" runat="server" BorderStyle="Solid" BorderWidth="2px" Width="655px">
    &nbsp;<asp:Label ID="ControlPanelLabel" runat="server" Text="Control" Font-Bold="True"></asp:Label>
        <br />
        <span style="padding-left:57px">
            <asp:Button ID="AddItemButton" runat="server" Text="Add/Insert Schedule Item" OnClick="AddItemButton_Click" Width="244px" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="EditItemButton" runat="server" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Text="Edit Selected Item" Width="244px" OnClick="EditItemButton_Click" />
        </span>
        <br />
        <br />
        <span style="padding-left:57px">
            <asp:Button ID="DeleteItemButton" runat="server" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" OnClick="DeleteItemButton_Click" Text="Delete Selected Item" Width="244px" />
        <br />
        </span>
        <br />
        <span style="padding-left:20px">
        <asp:Label ID="StatusLabel" runat="server" Text="" Font-Bold="True" ForeColor="Red"></asp:Label>
        </span>
    </asp:Panel>

    <asp:UpdatePanel ID="AppendInsertUpdatePanel" runat="server">
    <ContentTemplate>
    <asp:Panel ID="AddEntryPanel" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="2px" Width="655px">
        &nbsp;<asp:Label ID="HeaderLabel" runat="server" Text="Add/Insert New Schedule Entry " Font-Bold="True"></asp:Label>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="DayNumLabel" runat="server" Text="Day of Week:" Font-Bold="False" Font-Size="Small" Width="140px"></asp:Label>&nbsp;
            <asp:DropDownList ID="DayDropDownList" runat="server" Font-Size="Small" Height="25px" Width="110px">
                <asp:ListItem Value="0">Sunday</asp:ListItem>
                <asp:ListItem Value="1">Monday</asp:ListItem>
                <asp:ListItem Value="2">Tuesday</asp:ListItem>
                <asp:ListItem Value="3">Wednesday</asp:ListItem>
                <asp:ListItem Value="4">Thursday</asp:ListItem>
                <asp:ListItem Value="5">Friday</asp:ListItem>
                <asp:ListItem Value="6">Saturday</asp:ListItem>
            </asp:DropDownList>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="StartTimeLabelHour" runat="server" Text="Start Time (Hour: 0-23):" Font-Bold="False" Font-Size="Small" Width="140px"></asp:Label>&nbsp;
            <asp:DropDownList ID="StartTimeHour" runat="server" Font-Size="Small" Height="25px" Width="110px">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="StartTimeLabelMinute" runat="server" Font-Bold="False" Text="Start Time (Min: 0-59):" Font-Size="Small"></asp:Label>&nbsp;
            <asp:DropDownList ID="StartTimeMinute" runat="server" Font-Size="Small" Height="25px" Width="60px">
                <asp:ListItem>0</asp:ListItem>
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
                <asp:ListItem>4</asp:ListItem>
                <asp:ListItem>5</asp:ListItem>
                <asp:ListItem>6</asp:ListItem>
                <asp:ListItem>7</asp:ListItem>
                <asp:ListItem>8</asp:ListItem>
                <asp:ListItem>9</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
                <asp:ListItem>24</asp:ListItem>
                <asp:ListItem>25</asp:ListItem>
                <asp:ListItem>26</asp:ListItem>
                <asp:ListItem>27</asp:ListItem>
                <asp:ListItem>28</asp:ListItem>
                <asp:ListItem>29</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>31</asp:ListItem>
                <asp:ListItem>32</asp:ListItem>
                <asp:ListItem>33</asp:ListItem>
                <asp:ListItem>34</asp:ListItem>
                <asp:ListItem>35</asp:ListItem>
                <asp:ListItem>36</asp:ListItem>
                <asp:ListItem>37</asp:ListItem>
                <asp:ListItem>38</asp:ListItem>
                <asp:ListItem>39</asp:ListItem>
                <asp:ListItem>40</asp:ListItem>
                <asp:ListItem>41</asp:ListItem>
                <asp:ListItem>42</asp:ListItem>
                <asp:ListItem>43</asp:ListItem>
                <asp:ListItem>44</asp:ListItem>
                <asp:ListItem>45</asp:ListItem>
                <asp:ListItem>46</asp:ListItem>
                <asp:ListItem>47</asp:ListItem>
                <asp:ListItem>48</asp:ListItem>
                <asp:ListItem>49</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>51</asp:ListItem>
                <asp:ListItem>52</asp:ListItem>
                <asp:ListItem>53</asp:ListItem>
                <asp:ListItem>54</asp:ListItem>
                <asp:ListItem>55</asp:ListItem>
                <asp:ListItem>56</asp:ListItem>
                <asp:ListItem>57</asp:ListItem>
                <asp:ListItem>58</asp:ListItem>
                <asp:ListItem>59</asp:ListItem>
            </asp:DropDownList>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="PeriodDescriptionLabel" runat="server" Text="Period Description:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="PeriodDescription" runat="server" BackColor="White" BorderColor="Black" Width="125" Visible="True" Font-Size="Small"></asp:TextBox>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="TypeLabel" runat="server" Text="Type:" Font-Bold="False" Font-Size="Small"></asp:Label>&nbsp;
            <asp:DropDownList ID="TypeDropDownList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="TypeDropDownList_SelectedIndexChanged" Font-Size="Small" Height="25px">
                <asp:ListItem Value="1">News</asp:ListItem>
                <asp:ListItem Value="2">Sports</asp:ListItem>
                <asp:ListItem Value="3">Weather</asp:ListItem>
                <asp:ListItem Value="4">Financial</asp:ListItem>
                <asp:ListItem Value="5">RSS (All)</asp:ListItem>
            </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="GroupLabel" runat="server" Text="Group:" Font-Bold="False" Font-Size="Small" Visible="False"></asp:Label>&nbsp;
            <asp:DropDownList ID="GroupDropDownList" runat="server" OnSelectedIndexChanged="TypeDropDownList_SelectedIndexChanged" Font-Size="Small" Height="25px" Visible="False">
            </asp:DropDownList>
        </span>
        <br />
        <span style="padding-left:20px">
            &nbsp;
            </span>
        <br />
        <span style="padding-left:20px">
            <asp:Button ID="AddEntryButton" runat="server" Text="Add Entry to Schedule" OnClick="AddEntryButton_Click" BackColor="Silver" Width="200px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />&nbsp;
            <asp:Button ID="InsertEntryButton" runat="server" Text="Insert Entry into Schedule" OnClick="InsertEntryButton_Click" BackColor="Silver" Width="220px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />&nbsp;
            <asp:Button ID="CancelButton" runat="server" OnClick="CancelButton_Click" Text="Cancel" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Width="80px" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="ConfirmLabel" runat="server" Font-Bold="True" Text="Entry saved to database" Visible="False" ForeColor="#33CC33"></asp:Label>
        </span>
        <br />
        <asp:EntityDataSource ID="dsDataBug" runat="server" ConnectionString="name=FoxPlusEntities" DefaultContainerName="FoxPlusEntities" EnableFlattening="False" EntitySetName="BugDataTypeSchedules">
        </asp:EntityDataSource>
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="EditUpdatePanel" runat="server">
    <ContentTemplate>
    <asp:Panel ID="EditEntryPanel" runat="server" Visible="False" BorderStyle="Solid" BorderWidth="2px" Width="655px">
        &nbsp;<asp:Label ID="HeaderLabel0" runat="server" Text="Edit Schedule Entry " Font-Bold="True"></asp:Label>
        <br />
        <br />
        <span style="padding-left:20px">
        <asp:Label ID="PeriodDescriptionLabelEdit" runat="server" Font-Bold="False" Font-Size="Small" Text="Period Description:"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="PeriodDescriptionEdit" runat="server" BackColor="White" BorderColor="Black" Font-Size="Small" Visible="True" Width="125"></asp:TextBox>
        </span>
        <br />
        <br />
        <span style="padding-left:20px">
        <asp:Label ID="Label1" runat="server" Font-Bold="False" Font-Size="Small" Text="Type:"></asp:Label>
        &nbsp;
        <asp:DropDownList ID="TypeDropDownListEdit" runat="server" Font-Size="Small" Height="25px" OnSelectedIndexChanged="TypeDropDownListEdit_SelectedIndexChanged1" AutoPostBack="True">
            <asp:ListItem Value="1">News</asp:ListItem>
            <asp:ListItem Value="2">Sports</asp:ListItem>
            <asp:ListItem Value="3">Weather</asp:ListItem>
            <asp:ListItem Value="4">Financial</asp:ListItem>
            <asp:ListItem Value="5">RSS (All)</asp:ListItem>
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="GroupLabelEdit" runat="server" Font-Bold="False" Font-Size="Small" Text="Group:" Visible="False"></asp:Label>
        &nbsp;
        <asp:DropDownList ID="GroupDropDownListEdit" runat="server" Font-Size="Small" Height="25px" OnSelectedIndexChanged="TypeDropDownList_SelectedIndexChanged" Visible="False">
        </asp:DropDownList>
        </span>
        <br />
        <span style="padding-left:20px">
            &nbsp;
            </span>
        <br />
        <span style="padding-left:20px">
            <asp:Button ID="UpdatEntryButton" runat="server" Text="Update Entry" OnClick="UpdatEntryButton_Click" BackColor="Silver" Width="200px" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="CancelEditButton" runat="server" OnClick="CancelEditButton_Click" Text="Cancel" BackColor="Silver" BorderColor="Black" BorderStyle="Outset" BorderWidth="2px" Width="80px" />
        </span>
        <br />
        <span style="padding-left:20px">
            <asp:Label ID="EditConfirmLabel" runat="server" Font-Bold="True" Text="Entry saved to database" ForeColor="#33CC33"></asp:Label>
        </span>
        <br />
    </asp:Panel>
    </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:UpdatePanel ID="DayaSelectUpdatePanel" runat="server">
    <ContentTemplate>
    <span style="padding-left:0px">
        <asp:Label ID="DaySelectLabel" runat="server" Text="Day Select" Font-Bold="True"></asp:Label>&nbsp;
        <asp:DropDownList ID="DaySelectDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DataSelectDropDownSelectedIndexChanged">
            <asp:ListItem Value="0">All</asp:ListItem>
            <asp:ListItem Value="1">Sunday</asp:ListItem>
            <asp:ListItem Value="2">Monday</asp:ListItem>
            <asp:ListItem Value="3">Tuesday</asp:ListItem>
            <asp:ListItem Value="4">Wednesday</asp:ListItem>
            <asp:ListItem Value="5">Thursday</asp:ListItem>
            <asp:ListItem Value="6">Friday</asp:ListItem>
            <asp:ListItem Value="7">Saturday</asp:ListItem>
        </asp:DropDownList>
    </span>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="GridPanel" runat="server">
    <ContentTemplate>
    <asp:GridView ID="L3Grid" runat="server" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" Caption="Current Lower-Third Schedule" CaptionAlign="Left" CellPadding="4" Font-Bold="True" Font-Names="Arial" Font-Size="Medium" ForeColor="Black" Width="780px" AutoGenerateColumns="False" OnRowDataBound="L3Grid_RowDataBound" OnSelectedIndexChanged="L3Grid_SelectedIndexChanged">
        <HeaderStyle BackColor="Black" BorderColor="White" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" Font-Names="Arial" Font-Size="Small" ForeColor="White" HorizontalAlign="Center" />
        <AlternatingRowStyle BorderStyle="Solid" BackColor="Silver" />
            <Columns>
                <asp:CommandField ShowSelectButton="True">
                <ControlStyle Font-Size="Small" />
                <HeaderStyle Font-Size="Small" />
                <ItemStyle Width="120px" />
                </asp:CommandField>
                <asp:BoundField DataField="EntryIndex" HeaderText="Index" SortExpression="EntryIndex">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="DayName" HeaderText="Day" SortExpression="DayName">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="StartEnableTime" DataFormatString="{0: H:mm:ss}" HeaderText="Start Time" SortExpression="StartEnableTime">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="PeriodDescription" HeaderText="Period" SortExpression="PeriodDescription">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="EntryDataType" HeaderText="Type" SortExpression="EntryDataType">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="EntryDataDescription" HeaderText="Description" SortExpression="EntryDataDescription">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="EntryGroupID" HeaderText="Group ID" SortExpression="EntryGroupID" Visible="false">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
                <asp:BoundField DataField="GroupName" HeaderText="Group Description" SortExpression="GroupName">
                <HeaderStyle Font-Size="Small" />
                </asp:BoundField>
            </Columns>
        <EditRowStyle Font-Size="Small" />
        <FooterStyle BackColor="#CCCCCC" />
        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Center" />
        <RowStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="2px" Font-Bold="False" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="Gray" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#383838" />
    </asp:GridView>    
    </ContentTemplate>
    </asp:UpdatePanel>
    </asp:Content>
