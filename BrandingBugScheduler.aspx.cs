﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace WebApplication2
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        //Define connection string
        string connectionString = WebConfigurationManager.ConnectionStrings["FoxPlus"].ConnectionString;
        //string connectionString = "Data Source=OWNER-PC\\SQLEXPRESS;Initial Catalog=FoxPlus;Integrated Security=True;User ID=sa;Password=Vds@dmin1";

        //Function to refresh the branding bug grid
        protected void RefreshBrandingBugGrid()
        {
            //Setup connection, query and reader objects
            SqlConnection foxPlusConnection = new SqlConnection(connectionString);
            SqlCommand foxPlusQuery = new SqlCommand();
            SqlDataReader foxPlusReader;

            //Setup query & setup parameters based on UI controls
            foxPlusQuery.Connection = foxPlusConnection;
            string insertSQL;
            insertSQL = "sp_GetBrandingBugPlaylistInfoForEdit";

            foxPlusQuery.CommandText = insertSQL;

            //Open connection
            foxPlusConnection.Open();

            //Fire the reader
            foxPlusReader = foxPlusQuery.ExecuteReader();

            //Refresh the grid
            BrandingBugGrid.DataSource = foxPlusReader;
            BrandingBugGrid.DataBind();

            //Set the grid font sizes
            BrandingBugGrid.HeaderStyle.Font.Size = FontUnit.XSmall;

            //Clean up
            foxPlusReader.Close();
            foxPlusConnection.Close();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Enable/disable controls based on user rights
            if ((User.IsInRole("Administrator")) || (User.IsInRole("BrandingBugScheduleEditor")))
            {
                //Clear label & enable controls
                StatusLabel.Text = "";
                StatusLabel.ForeColor = System.Drawing.Color.LimeGreen;

                CntrlPanel.Enabled = true;
                BrandingBugGrid.Visible = true;
                BrandingBugGrid.Enabled = true;
            }
            else
            {
                //Force user to login page
                Response.Redirect("~/Account/Login.aspx");
                //Set label & disable controls
                //StatusLabel.ForeColor = System.Drawing.Color.Red;
                //StatusLabel.Text = "You are not logged in!";

                //CntrlPanel.Enabled = false;
                //BrandingBugGrid.Visible = false;
                //BrandingBugGrid.Enabled = false;
            }

            //Refresh the data grid
            RefreshBrandingBugGrid();

            if (!IsPostBack)
            {
                StartTimeHour.SelectedIndex = 0;
                StartTimeMinute.SelectedIndex = 0;

                //Init labels
                EditConfirmLabel.ForeColor = System.Drawing.Color.Red;
                EditConfirmLabel.Text = "No record selected for edit";
            }
        }

        //Color the grid in accordance with the color values set in the schedule
        protected void BrandingBugGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Font.Size = FontUnit.Small;

                //Right-aling Theme start time
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;

                Int16 colorval = (Int16)DataBinder.Eval(e.Row.DataItem, "ColorValue");
                switch (colorval)
                {
                    case 0:
                        e.Row.BackColor = System.Drawing.Color.Red;
                        break;
                    case 1:
                        e.Row.BackColor = System.Drawing.Color.CornflowerBlue;
                        break;
                    case 2:
                        e.Row.BackColor = System.Drawing.Color.Silver;
                        break;
                }
            }
        }

        //Handler for add item button
        protected void AddItemButton_Click(object sender, EventArgs e)
        {
            //Show panel for add/insert
            this.AddEntryPanel.Visible = true;
            //Make sure edit panel is not visible
            this.EditEntryPanel.Visible = false;
            //Clear status labels
            StatusLabel.Text = "";
            ConfirmLabel.Text = "";
            EditConfirmLabel.Text = "";
        }

        //Show & hide controls based on type of entry selected
        protected void TypeDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TypeDropDownList.SelectedIndex)
            {
                case 0:
                    TextLabel.Visible = true;
                    TextTextBox.Visible = true;
                    TimezoneLabel.Visible = false;
                    TimezoneDropDownList.Visible = false;
                    break;
                case 1:
                    TextLabel.Visible = false;
                    TextTextBox.Visible = false;
                    TimezoneLabel.Visible = true;
                    TimezoneDropDownList.Visible = true;
                    break;
            }
        }

        //Handler for adding a schedule entry
        protected void AddEntryButton_Click(object sender, EventArgs e)
        {
            //Check for valid data entry
            if (StartTimeHour.Text == "" || StartTimeMinute.Text == "" || ((TextTextBox.Text == "") && (TypeDropDownList.SelectedIndex == 0)))
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must enter valid data for all fields!";
                return;
            }

            //Get values needed from stored procedure from grid 
            int dayNumber = DayDropDownList.SelectedIndex + 1;
            string startEnableTime = "1899-12-30 " + StartTimeHour.SelectedItem.Text + ":" + StartTimeMinute.SelectedItem.Text + ":0";
            int colorValue = ColorDropDownList.SelectedIndex;
            string textLabel = TextTextBox.Text;
            bool clockEnable;
            string timeZoneSuffix;
            //Clock
            if (TypeDropDownList.SelectedIndex == 1)
            {
                clockEnable = true;
                timeZoneSuffix = TimezoneDropDownList.SelectedItem.Text;
            }
            //Simple text
            else
            {
                clockEnable = false;
                timeZoneSuffix = "";
            }
            //bool enabled = Convert.ToBoolean(BrandingBugGrid.Rows[rowNum].Cells[8].Text);
            bool enabled = EnableCheckBox.Checked;

            //Call stored procedure to add the row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_AppendBrandingBugScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += enabled + ", ";
                insertSQL += colorValue + ", ";
                insertSQL += "'" + textLabel + "', ";
                insertSQL += clockEnable + ", ";
                insertSQL += "'" + timeZoneSuffix + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the data grid
                RefreshBrandingBugGrid();

                //Alert operator
                StatusLabel.Text = "";
                ConfirmLabel.Text = "Entry appended to database";
                ConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                ConfirmLabel.Visible = true;

                //Un-select the row in the grid & clear the text
                BrandingBugGrid.SelectedIndex = -1;
                //Clear control values
                TextTextBox.Text = "";
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for inserting a schedule entry
        protected void InsertEntryButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (BrandingBugGrid.SelectedRow == null)
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must select a row in the grid before attempting to insert a new entry!";
                return;
            }

            //Check for valid data entry
            if ((TextTextBox.Text == "") && (TypeDropDownList.SelectedIndex == 0))
            {
                ConfirmLabel.ForeColor = System.Drawing.Color.Red;
                ConfirmLabel.Text = "You must enter valid data for all fields!";
                return;
            }

            //Get selected row index for grid
            int rowNum = BrandingBugGrid.SelectedRow.RowIndex+1;

            //Get values needed from stored procedure from grid 
            int entryIndex = Convert.ToInt32(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[1].Text);
            int dayNumber = DayDropDownList.SelectedIndex + 1;
            string startEnableTime = "1899-12-30 " + StartTimeHour.SelectedItem.Text + ":" + StartTimeMinute.SelectedItem.Text + ":0";
            int colorValue = ColorDropDownList.SelectedIndex;
            string textLabel = TextTextBox.Text;
            bool clockEnable;
            string timeZoneSuffix;
            //Clock
            if (TypeDropDownList.SelectedIndex == 1)
            {
                clockEnable = true;
                timeZoneSuffix = TimezoneDropDownList.SelectedItem.Text;
            }
            //Simple text
            else
            {
                clockEnable = false;
                timeZoneSuffix = "";
            }
            //bool enabled = Convert.ToBoolean(BrandingBugGrid.Rows[rowNum].Cells[8].Text);
            bool enabled = EnableCheckBox.Checked;

            //Call stored procedure to delete the selected row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_InsertBrandingBugScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += entryIndex + ", ";
                insertSQL += enabled + ", ";
                insertSQL += colorValue + ", ";
                insertSQL += "'" + textLabel + "', ";
                insertSQL += clockEnable + ", ";
                insertSQL += "'" + timeZoneSuffix + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the data grid
                RefreshBrandingBugGrid();

                //Alert operator
                StatusLabel.Text = "";
                ConfirmLabel.Text = "Entry inserted into database";
                ConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                ConfirmLabel.Visible = true;

                //Un-select the row in the grid & clear the text
                BrandingBugGrid.SelectedIndex = -1;
                //Clear control values
                TextTextBox.Text = "";
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for update (edit) selected entry button
        protected void UpdatEntryButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (BrandingBugGrid.SelectedRow == null)
            {
                EditConfirmLabel.ForeColor = System.Drawing.Color.Red;
                EditConfirmLabel.Text = "You must select a row in the grid before attempting to update an entry!";
                return;
            }

            //Check for valid data entry
            if ((TextTextBoxEdit.Text == "") && (TypeDropDownListEdit.SelectedIndex == 0))
            {
                EditConfirmLabel.ForeColor = System.Drawing.Color.Red;
                EditConfirmLabel.Text = "You must enter valid data for all fields!";
                return;
            }

            //Get selected row index for grid
            int rowNum = BrandingBugGrid.SelectedRow.RowIndex + 1;

            //Get values needed from stored procedure from grid 
            int entryIndex = rowNum;
            
            int dayNumber = 0;
            //int dayNumber = Convert.ToInt32(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text);
            if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Sun")
            {
                dayNumber = 1;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Mon")
            {
                dayNumber = 2;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Tue")
            {
                dayNumber = 3;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Wed")
            {
                dayNumber = 4;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Thu")
            {
                dayNumber = 5;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Fri")
            {
                dayNumber = 6;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Sat")
            {
                dayNumber = 7;
            }           
            
            string startEnableTime = "1899-12-30 " +
                Convert.ToString(Convert.ToDateTime(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[3].Text).Hour) + ":" +
                Convert.ToString(Convert.ToDateTime(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[3].Text).Minute) + ":0";
            int colorValue = ColorDropDownListEdit.SelectedIndex;
            string textLabel = TextTextBoxEdit.Text;
            bool clockEnable;
            string timeZoneSuffix;
            //Clock
            if (TypeDropDownListEdit.SelectedIndex == 1)
            {
                clockEnable = true;
                timeZoneSuffix = TimezoneDropDownListEdit.SelectedItem.Text;
                textLabel = "";
            }
            //Simple text
            else
            {
                clockEnable = false;
                timeZoneSuffix = "";
            }
            
            bool enabled = EnableCheckBoxEdit.Checked;

            //Call stored procedure to delete the selected row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_UpdateBrandingBugScheduleEntry ";
                insertSQL += dayNumber + ", ";
                insertSQL += "'" + startEnableTime + "', ";
                insertSQL += entryIndex + ", ";
                insertSQL += enabled + ", ";
                insertSQL += colorValue + ", ";
                insertSQL += "'" + textLabel + "', ";
                insertSQL += clockEnable + ", ";
                insertSQL += "'" + timeZoneSuffix + "'";

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the data grid
                RefreshBrandingBugGrid();

                //Alert operator
                StatusLabel.Text = "";
                EditConfirmLabel.Text = "Edited entry saved to database. You can select another row to edit.";
                EditConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
                EditConfirmLabel.Visible = true;

                //Un-select the row in the grid & clear the text
                BrandingBugGrid.SelectedIndex = -1;
                TextTextBoxEdit.Text = "";
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for Delete Selected Entry button
        protected void DeleteItemButton_Click(object sender, EventArgs e)
        {
            //Check for null value then get index of selected row
            if (BrandingBugGrid.SelectedRow == null)
            {
                StatusLabel.ForeColor = System.Drawing.Color.Red;
                StatusLabel.Text = "You must select a row in the grid before attempting to delete an entry!";
                return;
            }

            //Get selected row index for grid
            int rowNum = BrandingBugGrid.SelectedRow.RowIndex;

            //Get values needed from stored procedure from grid 
            string entryIndex = BrandingBugGrid.Rows[rowNum].Cells[1].Text;

            //string dayNumber = BrandingBugGrid.Rows[rowNum].Cells[2].Text;
            int dayNumber = 0;
            //int dayNumber = Convert.ToInt32(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text);
            if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Sun")
            {
                dayNumber = 1;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Mon")
            {
                dayNumber = 2;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Tue")
            {
                dayNumber = 3;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Wed")
            {
                dayNumber = 4;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Thu")
            {
                dayNumber = 5;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Fri")
            {
                dayNumber = 6;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[2].Text == "Sat")
            {
                dayNumber = 7;
            }           

            string startEnableTime = BrandingBugGrid.Rows[rowNum].Cells[3].Text;

            //Call stored procedure to delete the selected row
            try
            {
                //Setup conection and query objects
                SqlConnection foxPlusConnection = new SqlConnection(connectionString);
                SqlCommand foxPlusQuery = new SqlCommand();

                //Setup query & setup parameters based on UI controls.
                foxPlusQuery.Connection = foxPlusConnection;
                string insertSQL;
                insertSQL = "sp_DeleteBrandingBugScheduleEntry ";
                insertSQL += dayNumber + ", ";
                //insertSQL += "1" + ", ";
                insertSQL += "'" + "1899-12-30 " + startEnableTime + "', ";
                insertSQL += entryIndex;

                foxPlusQuery.CommandText = insertSQL;

                //Open connection
                foxPlusConnection.Open();

                foxPlusQuery.ExecuteNonQuery();

                //Dispose of the connection
                foxPlusConnection.Close();

                //Refresh the data grid
                RefreshBrandingBugGrid();

                //Alert operator & clear other labels
                StatusLabel.Text = "Entry deleted from database";
                StatusLabel.ForeColor = System.Drawing.Color.LimeGreen;
                StatusLabel.Visible = true;
                ConfirmLabel.Text = "";
                EditConfirmLabel.Text = "";

                //Clear control values
                TextTextBox.Text = "";
                //Un-select the row in the grid & clear the text
                BrandingBugGrid.SelectedIndex = -1;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.NullReferenceException err)
            {
                StatusLabel.Text = err.Message;
            }
            catch (System.Exception err)
            {
                StatusLabel.Text = err.Message;
            }
        }

        //Handler for edit item button
        protected void EditItemButton_Click(object sender, EventArgs e)
        {
            //Show panel for edit
            this.EditEntryPanel.Visible = true;
            //Make sure add/insert panel is not visible
            this.AddEntryPanel.Visible = false;
            //Clear status label
            StatusLabel.Text = "";
            ConfirmLabel.Text = "";
            EditConfirmLabel.Text = "";
        }

        //Handler for cancel add/insert entry button
        protected void CancelAddInsertButton_Click(object sender, EventArgs e)
        {
            AddEntryPanel.Visible = false;
            ConfirmLabel.Text = "";
            //Deselect entry in grid
            BrandingBugGrid.SelectedIndex = -1;
            return;
        }

        //Handler for cancel edit entry button
        protected void CancelEditButton_Click(object sender, EventArgs e)
        {
            EditEntryPanel.Visible = false;
            EditConfirmLabel.Text = "";
            //Deselect entry in grid
            BrandingBugGrid.SelectedIndex = -1;
            return;
        }

        //Handler for  change to selected row in grid - used when in update mode
        protected void BrandingBugGrid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Set edit panel control values based on currently selected record
            if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[4].Text == "Red")
            {
                ColorDropDownListEdit.SelectedIndex = 0;
            }
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[4].Text == "Blue")
            {
                ColorDropDownListEdit.SelectedIndex = 1;
            } 
            else if (BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[4].Text == "Gray")
            {
                ColorDropDownListEdit.SelectedIndex = 2;
            }
            //ColorDropDownListEdit.SelectedIndex = Convert.ToInt32(BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[4].Text);
            CheckBox chk = BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[6].Controls[0] as CheckBox;
            //Check if data type is clock
            if (chk != null && chk.Checked)
            {
                TextTextBoxEdit.Visible = false;
                TextLabelEdit.Visible = false;
                TimezoneDropDownListEdit.Visible = true;
                TimezoneLabelEdit.Visible = true;

                TypeDropDownListEdit.SelectedIndex = 1;
                TextTextBoxEdit.Text = "";
                string timeZoneStr = BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[7].Text;
                if (timeZoneStr == "ET") 
                {
                    TimezoneDropDownListEdit.SelectedIndex = 0;
                }
                else if (timeZoneStr == "CT") 
                {
                    TimezoneDropDownListEdit.SelectedIndex = 1;
                }
                else if (timeZoneStr == "MT") 
                {
                    TimezoneDropDownListEdit.SelectedIndex = 2;
                }
                else if (timeZoneStr == "PT") 
                {
                    TimezoneDropDownListEdit.SelectedIndex = 3;
                }
            }
            else
            {
                TextTextBoxEdit.Visible = true;
                TextLabelEdit.Visible = true;
                TimezoneDropDownListEdit.Visible = false;
                TimezoneLabelEdit.Visible = false;

                TypeDropDownListEdit.SelectedIndex = 0;
                TextTextBoxEdit.Text = BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[5].Text;
            }
            CheckBox chk2 = BrandingBugGrid.Rows[BrandingBugGrid.SelectedIndex].Cells[8].Controls[0] as CheckBox;
            //Check if data type is clock
            if (chk2 != null && chk2.Checked)
            {
                EnableCheckBoxEdit.Checked = true;
            }
            else
            {
                EnableCheckBoxEdit.Checked = false;
            }
            //Set confirm label
            EditConfirmLabel.ForeColor = System.Drawing.Color.LimeGreen;
            EditConfirmLabel.Text = "Record selected for edit";
        }

        //Handler for change to type drop-down list in edit mode
        protected void TypeDropDownListEdit_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (TypeDropDownListEdit.SelectedIndex)
            {
                case 0:
                    TextLabelEdit.Visible = true;
                    TextTextBoxEdit.Visible = true;
                    TimezoneLabelEdit.Visible = false;
                    TimezoneDropDownListEdit.Visible = false;
                    break;
                case 1:
                    TextLabelEdit.Visible = false;
                    TextTextBoxEdit.Visible = false;
                    TimezoneLabelEdit.Visible = true;
                    TimezoneDropDownListEdit.Visible = true;
                    break;
            }
        }

        //Handler for change in index to day select drop-down => refreshes grid filtered for selected day
        protected void DayaSelectDropDownSelectedIndexChanged(object sender, EventArgs e)
        {
            //Setup connection, query and reader objects
            SqlConnection foxPlusConnection = new SqlConnection(connectionString);
            SqlCommand foxPlusQuery = new SqlCommand();
            SqlDataReader foxPlusReader;

            //Setup query & setup parameters based on UI controls
            foxPlusQuery.Connection = foxPlusConnection;
            string insertSQL;
            //Request data for grid - filter for selected day
            insertSQL = "sp_GetBrandingBugPlaylistInfoForEdit " + Convert.ToString(DaySelectDropDown.SelectedIndex);

            foxPlusQuery.CommandText = insertSQL;

            //Open connection
            foxPlusConnection.Open();

            //Fire the reader
            foxPlusReader = foxPlusQuery.ExecuteReader();

            //Refresh the grid
            BrandingBugGrid.DataSource = foxPlusReader;
            BrandingBugGrid.DataBind();

            //Set the grid font sizes
            BrandingBugGrid.HeaderStyle.Font.Size = FontUnit.XSmall;

            //Clean up
            foxPlusReader.Close();
            foxPlusConnection.Close();
        }
    }
}