﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Data.SqlClient;

namespace WebApplication2.Account
{
    public partial class Login : Page
    {
        //Define connection string
        string connectionString = WebConfigurationManager.ConnectionStrings["FoxPlus"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {

            //RegisterHyperLink.NavigateUrl = "Register.aspx";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];

            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                //RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        //Handler to post user login to SQL database
        protected void LoggedIn(object sender, EventArgs e)
        {
            Session["User"] = LoginMod.UserName;

            //Call stored procedure to add the record
            //Setup conection and query objects
            SqlConnection foxPlusConnection = new SqlConnection(connectionString);
            SqlCommand foxPlusQuery = new SqlCommand();

            //Setup query & setup parameters based on UI controls.
            foxPlusQuery.Connection = foxPlusConnection;
            string insertSQL;
            insertSQL = "sp_PostSchedulerLoginLogEntry ";
            insertSQL += LoginMod.UserName + ", ";
            //Set flag to indicate login
            insertSQL += "1";

            foxPlusQuery.CommandText = insertSQL;

            //Open connection
            foxPlusConnection.Open();

            foxPlusQuery.ExecuteNonQuery();

            //Dispose of the connection
            foxPlusConnection.Close();
        }

        protected void Unnamed5_Click(object sender, EventArgs e)
        {
            
        }

    }
}