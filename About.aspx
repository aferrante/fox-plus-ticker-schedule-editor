﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="WebApplication2.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1>Fox Plus Ticker Schedule Editor</h1>
    </hgroup>

    <article>
        <p>        
            Revision: 1.0.2&nbsp; 15-February-2013&nbsp; 18:00</p>

        <p>        
            Copyright 2013, Video Design Software Inc.
        </p>

        <p>        
            All rights reserved.</p>
    </article>

    </asp:Content>

