﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Enable/disable controls based on user rights
            if ((User.IsInRole("Administrator")) || 
                (User.IsInRole("BrandingBugScheduleEditor")) || 
                (User.IsInRole("DataBugScheduleEditor")) || 
                (User.IsInRole("L3ContentScheduleEditor")))
            {
                //Clear label & enable controls
                StatusLabel.Text = "";
                StatusLabel.ForeColor = System.Drawing.Color.LimeGreen;                
            }
            else
            {
                //Force user to login page
                Response.Redirect("~/Account/Login.aspx");
                //Set label & disable controls
                //StatusLabel.BackColor = System.Drawing.Color.Red;
                //StatusLabel.ForeColor = System.Drawing.Color.White;
                //StatusLabel.Text = "You are not logged in!";
            }
        }
    }
}